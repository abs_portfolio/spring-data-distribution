/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.dao_service;

import com.distribution.net.beans.User;
import com.distribution.net.controllers.exceptions.UserNotFound;
import com.distribution.net.controllers.exceptions.UuidNotDefined;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Component;

/**
 *
 * @author onelove
 */
@Component
public class UserDaoService {

    private static List<User> users = new ArrayList();

    static {
        try {
            users.add(new User(UUID.randomUUID(), "George", new SimpleDateFormat("dd/MM/yyyy").parse("23/09/1994")));
            users.add(new User(UUID.randomUUID(), "Kwstas", new SimpleDateFormat("dd/MM/yyyy").parse("23/11/1894")));
            users.add(new User(UUID.randomUUID(), "Nikos", new SimpleDateFormat("dd/MM/yyyy").parse("25/10/1995")));
        } catch (ParseException ex) {
            System.err.println("Error on parsing static dates for dammy user objects.");
        }
    }

    public List<User> findAll() {
        return users;
    }

    public User save(User user) {
        if (user.getUuid() == null) {
            user.setUuid(UUID.randomUUID());
        }
        users.add(user);
        return user;
    }

    public User findOne(UUID id) throws UserNotFound, UuidNotDefined{
        if(id == null){
            throw new UuidNotDefined("User id was not defined in the request path.");
        }
        for (User user : users) {
            if (user.getUuid().toString().equals(id.toString())) {
                return user;
            }
        }
        throw new UserNotFound("We were not able to identify the requested User.");
    }
    
    public User deleteById(UUID uuid) throws UserNotFound{
        Iterator<User> iterator = users.iterator();
        while(iterator.hasNext()){
            User user = iterator.next();
            if(user.getUuid().toString().equals(uuid.toString())){
                iterator.remove();
                return user;
            }
        }
        throw new UserNotFound("We were not able to identify the requested User.");
    }
}
