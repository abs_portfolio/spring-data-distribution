/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.versioning.beans;

/**
 *
 * @author onelove
 */
public class PersonV2 {

    private Name fullname;

    public PersonV2(Name fullname) {
        this.fullname = fullname;
    }

    public PersonV2() {
    }

    public Name getFullname() {
        return fullname;
    }

    public void setFullname(Name fullname) {
        this.fullname = fullname;
    }

}
