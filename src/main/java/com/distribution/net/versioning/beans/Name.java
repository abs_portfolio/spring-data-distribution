/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.versioning.beans;

/**
 *
 * @author onelove
 */
public class Name {

    private String name;
    private String surename;

    public String getName() {
        return name;
    }

    public String getSurename() {
        return surename;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurename(String surename) {
        this.surename = surename;
    }

    public Name(String name, String surename) {
        this.name = name;
        this.surename = surename;
    }

    public Name() {
    }

}
