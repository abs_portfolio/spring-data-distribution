/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.versioning.service;

import com.distribution.net.beans.User;
import com.distribution.net.versioning.beans.Name;
import com.distribution.net.versioning.beans.PersonV1;
import com.distribution.net.versioning.beans.PersonV2;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author onelove
 */
@RestController
public class PersonResource {

    //First way of differencating
    //like http://localhost:8090/v1/person
    @RequestMapping(method = RequestMethod.GET, path = "/v1/person")
    public PersonV1 personV1() {
        return new PersonV1("Jony Dep");
    }

    @RequestMapping(method = RequestMethod.GET, path = "/v2/person")
    public PersonV2 personV2() {
        return new PersonV2(new Name("Jony", "Dep"));
    }

    //Second way of differencating
    //like : http://localhost:8090/person/param?version=1
    @RequestMapping(method = RequestMethod.GET, path = "/person/param", params = "version=1")
    public PersonV1 parmV1() {
        return new PersonV1("Jony Dep");
    }

    @RequestMapping(method = RequestMethod.GET, path = "/person/param", params = "version=2")
    public PersonV2 paramV2() {
        return new PersonV2(new Name("Jony", "Dep"));
    }

    //Third way of differencating
    //like : http://localhost:8090/person/header
    //headers : key : X-API-VERSION | value : 1
    @RequestMapping(method = RequestMethod.GET, path = "/person/header", headers = "X-API-VERSION=1")
    public PersonV1 headerV1() {
        return new PersonV1("Jony Dep");
    }

    @RequestMapping(method = RequestMethod.GET, path = "/person/header", headers = "X-API-VERSION=2")
    public PersonV2 headerV2() {
        return new PersonV2(new Name("Jony", "Dep"));
    }
    //Forth way of differencating
    //like : http://localhost:8090/person/produces
    //headers : key : Accept | value : application/vnd.company.app-v1+json
    @RequestMapping(method = RequestMethod.GET, path = "/person/produces", produces = "application/vnd.company.app-v1+json")
    public PersonV1 producesV1() {
        return new PersonV1("Jony Dep");
    }

    @RequestMapping(method = RequestMethod.GET, path = "/person/produces", produces = "application/vnd.company.app-v2+json")
    public PersonV2 producesV2() {
        return new PersonV2(new Name("Jony", "Dep"));
    }
}
