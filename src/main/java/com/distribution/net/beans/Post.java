/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author onelove
 */
@Entity
@Table(name = "post")
@ApiModel(description = "All users posts") //Model level configuration swagger
public class Post {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(columnDefinition = "BINARY(16)", name = "uuid", updatable = false, nullable = false)
    @ApiModelProperty(notes = "UUID format.") //swagger attribute level configuration (discription)
    private UUID uuid;
    //validation
    @Size(min = 2, message = "Name should have at least 2 characters.")
    @ApiModelProperty(notes = "Name should containe at least 2 characters.") //swagger attribute level configuration (discription)
    private String description;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JsonIgnore //to prevent the recursive print (Stack Overflow)
    private User user; 

    public Post(UUID uuid, String description, User user) {
        this.uuid = uuid;
        this.description = description;
        this.user = user;
    }

    public Post() {
    }

    public Post(String description, User user) {
        this.description = description;
        this.user = user;
    }

    
    public UUID getUuid() {
        return uuid;
    }

    public String getDescription() {
        return description;
    }

    public User getUser() {
        return user;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Post{" + "uuid=" + uuid + ", description=" + description + '}';
    }
    
    
}
