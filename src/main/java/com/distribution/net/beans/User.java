/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author onelove
 */
@Entity
@Table(name = "user")
@ApiModel(description = "All details about the user") //Model level configuration swagger
public class User {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(columnDefinition = "BINARY(16)", name = "uuid", updatable = false, nullable = false)
    @ApiModelProperty(notes = "UUID format.") //swagger attribute level configuration (discription)
    private UUID uuid;
    //validation
    @Size(min = 2, message = "Name should have at least 2 characters.")
    @ApiModelProperty(notes = "Name should containe at least 2 characters.") //swagger attribute level configuration (discription)
    private String name;

    @Past
    @ApiModelProperty(notes = "Birth date should be in the past.")
    private Date birth_date;

    //exclude attribute from json mapping
    @JsonIgnore
    private String password_hash;
    
    @OneToMany(mappedBy="user")
    private List<Post> posts;
    
    
    public User(String name, Date birth_date) {
        this.name = name;
        this.birth_date = birth_date;
    }

    public User(UUID uuid, String name, Date birth_date) {
        this.uuid = uuid;
        this.name = name;
        this.birth_date = birth_date;
    }

    public User() {
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
    

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }

    public String getPassword_hash() {
        return password_hash;
    }

    public void setPassword_hash(String password_hash) {
        this.password_hash = password_hash;
    }

    @Override
    public String toString() {
        return "User{" + "uuid=" + uuid.toString() + ", name=" + name + ", birth_date=" + birth_date + '}';
    }

}
