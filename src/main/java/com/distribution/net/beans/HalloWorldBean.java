/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.beans;

/**
 *
 * @author onelove
 */
public class HalloWorldBean {

    private String message;

    public HalloWorldBean(String message) {
        this.message = message;
    }

    public HalloWorldBean() {
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
