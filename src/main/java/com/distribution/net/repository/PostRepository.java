/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.repository;

import com.distribution.net.beans.Post;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author onelove
 */
public interface PostRepository extends CrudRepository<Post, UUID> {
    
}
