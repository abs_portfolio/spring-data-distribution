/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import com.distribution.net.beans.User;
import com.distribution.net.controllers.exceptions.UserNotFound;
import com.distribution.net.controllers.exceptions.UuidNotDefined;
import com.distribution.net.controllers.responses.Message;
import com.distribution.net.dao_service.UserDaoService;
import java.net.URI;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author onelove
 */
@RestController
@RequestMapping("/static")
public class UserResource {

    @Autowired
    private UserDaoService service;

    //retrieveAllUsers
    @RequestMapping(method = RequestMethod.GET, path = "/users")
    public List<User> findAllUssers() {
        return service.findAll();
    }

    //Get /users/{id}
    @RequestMapping(method = RequestMethod.GET, path = "/user/{uuid}")
    public Resource<User> findOneUser(@PathVariable String uuid) throws UserNotFound, UuidNotDefined {
        User user = service.findOne(UUID.fromString(uuid));

        // "all-users", SERVER_PATH + "/users"
        //retrieveAllUsers
        //hateoas
        Resource<User> resource = new Resource<>(user);
        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).findAllUssers());
        resource.add(linkTo.withRel("all-users"));
        
        return resource;
    }

    //POST /users/{id}
    //@Valid to validate the posted body attribute content
    @RequestMapping(method = RequestMethod.POST, path = "/user")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{uuid}")
                .buildAndExpand(service.save(user).getUuid())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    //Delete /users/{id}
    @RequestMapping(method = RequestMethod.DELETE, path = "/user/{uuid}")
    public ResponseEntity<Object> deleteUserById(@PathVariable String uuid) throws UserNotFound {
        service.deleteById(UUID.fromString(uuid));
        return ResponseEntity.status(200).body(new Message("ok"));
    }
}
