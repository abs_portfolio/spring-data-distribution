/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.controllers;

import com.distribution.net.beans.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author onelove
 */
@RestController
public class HalloContoller {

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(method = RequestMethod.GET, path = "/hello-world")
    public String helloWorld() {
        return "Hallo World";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/hello-world-bean")
    public HalloWorldBean helloWorldBean() {
        return new HalloWorldBean("Hallo World");
    }

    @RequestMapping(method = RequestMethod.GET, path = "/hello-world-var/{name}")
    public HalloWorldBean helloWorldBeanParse(@PathVariable String name) {
        return new HalloWorldBean(String.format("Hallo World %s", name));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/hello-world-internationalized")
    public String helloWorldInternationalizes() { //@RequestHeader(name = "Accept-Language", required = false) Locale locale
        return messageSource.getMessage("good.morning.message", null, LocaleContextHolder.getLocale());
    }

}
