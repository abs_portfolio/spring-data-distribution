/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.controllers;

import com.distribution.net.beans.Post;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import com.distribution.net.beans.User;
import com.distribution.net.controllers.exceptions.UserNotFound;
import com.distribution.net.controllers.exceptions.UuidNotDefined;
import com.distribution.net.controllers.responses.Message;
import com.distribution.net.repository.PostRepository;
import com.distribution.net.repository.UserRepository;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author onelove
 */
@RestController
@RequestMapping("/jpa")
public class UserJpaResource {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;

    //retrieveAllUsers
    @RequestMapping(method = RequestMethod.GET, path = "/users")
    public Iterable<User> findAllUssers() {
        return userRepository.findAll();
    }

    //Get /users/{id}
    @RequestMapping(method = RequestMethod.GET, path = "/users/{uuid}")
    public Resource<User> findOneUser(@PathVariable UUID uuid) throws UserNotFound, UuidNotDefined {
        Optional<User> user = userRepository.findById(uuid);

        if (!user.isPresent()) {
            throw new UserNotFound("We where not able to identify the user resource.");
        }
        // "all-users", SERVER_PATH + "/users"
        //retrieveAllUsers
        //hateoas
        Resource<User> resource = new Resource<>(user.get());
        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).findAllUssers());
        resource.add(linkTo.withRel("all-users"));

        return resource;
    }

    //POST /users/{id}
    //@Valid to validate the posted body attribute content
    @RequestMapping(method = RequestMethod.POST, path = "/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{uuid}")
                .buildAndExpand(userRepository.save(user).getUuid())
                .toUri();
        return ResponseEntity.created(location).build();
    }

    //Delete /users/{id}
    @RequestMapping(method = RequestMethod.DELETE, path = "/users/{uuid}")
    public ResponseEntity<Object> deleteUserById(@PathVariable UUID uuid) throws UserNotFound {
        userRepository.deleteById(uuid);
        return ResponseEntity.status(200).body(new Message("ok"));
    }

    /*----------------------------------posts section-------------------------------------*/
    
    //retrieve User posts
    @RequestMapping(method = RequestMethod.GET, path = "/users/{uuid}/posts")
    public List<Post> findAllPosts(@PathVariable UUID uuid) throws UserNotFound {
        Optional<User> user = userRepository.findById(uuid);

        if (!user.isPresent()) {
            throw new UserNotFound("We where not able to identify the user resource with id = " + uuid.toString());
        }
        return user.get().getPosts();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/users/{uuid}/posts/{post_id}")
    public Resource<Post> getUserPost(@PathVariable UUID uuid, @PathVariable UUID post_id) throws UserNotFound {
        Optional<User> user = userRepository.findById(uuid);
        Optional<Post> post = postRepository.findById(post_id);

        if (!user.isPresent()) {
            throw new UserNotFound("We where not able to identify the user resource with id = " + uuid.toString());
        }
        if (!post.isPresent()) {
            throw new UserNotFound("We where not able to identify the psot resource with id = " + post_id.toString());
        }
        Resource<Post> resource = new Resource<>(post.get());
        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).findAllPosts(uuid));
        resource.add(linkTo.withRel("all-user-posts"));
        return resource;
    }
    
    @RequestMapping(method = RequestMethod.POST, path = "/users/{uuid}/posts")
    public ResponseEntity<Object> createUserPost(@PathVariable UUID uuid, @RequestBody Post post) throws UserNotFound {
        Optional<User> user = userRepository.findById(uuid);

        if (!user.isPresent()) {
            throw new UserNotFound("We where not able to identify the user resource with id = " + uuid.toString());
        }

        post.setUser(user.get());
        
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{uuid}")
                .buildAndExpand(postRepository.save(post).getUuid())
                .toUri();
        return ResponseEntity.created(location).build();
    }
}
