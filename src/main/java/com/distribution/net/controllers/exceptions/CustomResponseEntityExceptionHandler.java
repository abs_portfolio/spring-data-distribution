/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.distribution.net.controllers.exceptions;

import java.util.Date;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author onelove
 */
@ControllerAdvice //shared accross multiple controller classes
@RestController //respose on exception handling
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        ExceptionResponseTemplate exception_tempalte
                = new ExceptionResponseTemplate(
                        new Date(),
                        ex.getMessage(),
                        request.getDescription(false)
                );
        return new ResponseEntity(exception_tempalte, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(UserNotFound.class)
    public final ResponseEntity<Object> handleUserNotFoundExceptions(UserNotFound ex, WebRequest request) {
        ExceptionResponseTemplate exception_tempalte
                = new ExceptionResponseTemplate(
                        new Date(),
                        ex.getMessage(),
                        request.getDescription(false)
                );
        return new ResponseEntity(exception_tempalte, HttpStatus.NOT_FOUND);
    }

    /**
     *
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(UuidNotDefined.class)
    public final ResponseEntity<Object> handleUuidNotDefinedExceptions(UuidNotDefined ex, WebRequest request) {
        ExceptionResponseTemplate exception_tempalte
                = new ExceptionResponseTemplate(
                        new Date(),
                        ex.getMessage(),
                        request.getDescription(false)
                );
        return new ResponseEntity(exception_tempalte, HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     *
     * @param ex
     * @param headers
     * @param status
     * @param request
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ExceptionResponseTemplate exception_tempalte
                = new ExceptionResponseTemplate(
                        new Date(),
                        "Validation Failed",
                        ex.getBindingResult().toString()
                );
        return new ResponseEntity(exception_tempalte, HttpStatus.BAD_REQUEST);
    }

}
