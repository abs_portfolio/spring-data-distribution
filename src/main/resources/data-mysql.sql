/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  onelove
 * Created: Nov 11, 2019
 */

/**
 * Author:  onelove
 * Created: Nov 11, 2019
 ALTER DATABASE [database] CHARACTER SET utf8 COLLATE utf8_unicode_ci;
*/
insert into user ( uuid, birth_date,name, password_hash ) values ( unhex(replace(uuid(),'-','')),sysdate(), 'George', 'mounaki');
insert into user ( uuid, birth_date,name, password_hash ) values ( unhex(replace(uuid(),'-','')),sysdate(), 'Mitsos', 'mounaki2');
insert into user ( uuid, birth_date,name, password_hash ) values ( unhex(replace(uuid(),'-','')),sysdate(), 'John', 'mounaki3');

insert into post  values (unhex(replace(uuid(),'-','')), 'First Post Ever', (select uuid from user where name = 'George'));
insert into post  values (unhex(replace(uuid(),'-','')), 'Second Post Ever', (select uuid from user where name = 'George'));
insert into post  values (unhex(replace(uuid(),'-','')), 'Third Post Ever', (select uuid from user where name = 'Mitsos'));